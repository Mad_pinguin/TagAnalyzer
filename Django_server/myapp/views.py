from fileinput import filename
from hashlib import new
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render
from django.http import JsonResponse
from .forms import UploadFileForm
from django.views.decorators.csrf import csrf_exempt
import os
import json
import pytesseract
#импорт сети
import shutil
import os
import cv2
import easyocr
from pyzbar.pyzbar import decode
import matplotlib.pyplot as plt
import zipfile
import datetime
import string
import glob
import math
import imutils
import random
import tqdm
import matplotlib.pyplot as plt
import tensorflow as tf
import sklearn.model_selection
import keras_ocr
from IPython.display import clear_output
import re
from PIL import Image
from Crypto.Cipher import AES
import base64
from .brom import *
from base64 import b64encode
from .Python1c import *
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import zxingcpp
import cv2
import imutils
import os
import zxingcpp
from PIL import Image, ImageOps
from cv2 import dnn_superres
from PIL import ImageFilter
import zbar
import zbar.misc

@csrf_exempt 
def upload_file(request):
    up_file = request.FILES['picture']
    pathToPhoto = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/content/' + up_file.name
    if os.path.exists(pathToPhoto):
            os.remove(pathToPhoto)
    destination = open(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/content/' + up_file.name, 'xb+')
    for chunk in up_file.chunks():
        destination.write(chunk)
    destination.close()
    
    result = new_fun(pathToPhoto, up_file.name)
    return JsonResponse(result)

def convertFindedToTrain(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)

def ocr_read(path, reader, isPrice):
    img = cv2.imread(path)
    img = imutils.resize(img, width=512)
    #if (isPrice==True):
    #    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #    ret, img = cv2.threshold(img, 162, 255, cv2.THRESH_BINARY)
    result = reader.readtext(img)
    text = ""
    for box in result:
        text = text + " " + box[1]
    #text = pytesseract.image_to_string(Image.open(path), lang = 'rus+eng',config='-c page_separator='' ')
    #text = pytesseract.image_to_string(Image.open("/home/sergei/runs/detect/predict/crops/Description/bin.png"), lang = 'rus+eng',config='-c page_separator='' ')
    text = text.replace("\n", "")
    return(text)

def new_fun(pathToPhoto, photoName):
    reader = easyocr.Reader(['ru','en'])
    # os.system("yolo task=detect mode=predict model=\"/home/sergei/YoloV8-best_combined.pt\" source=\"" + pathToPhoto + "\" save_crop save_txt")
    os.system("yolo task=detect mode=predict model=\"/home/sergei/Изображения/price tags/december_datasetupd/runs/detect/train/weights/LentaAshanBystronom_best.pt\" source=\"" + pathToPhoto + "\" save_crop save_txt")
    try:
        descriptionAnswer=ocr_read("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Description/"+photoName, reader, False)
    except:
        descriptionAnswer="Segment not found!"
    try:
        price11=ocr_read("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Price11/"+photoName, reader, True)
    except:
        price11="Segment not found!"
    try:
        price12=ocr_read("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Price12/"+photoName, reader, True)
    except:
        price12="Segment not found!"
    try:
        price21=ocr_read("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Price21/"+photoName, reader, True)
    except:
        price21="Segment not found!"
    try:
        price22=ocr_read("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Price22/"+photoName, reader, True)
    except:
        price22="Segment not found!"
    # sharpen("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/"+photoName, "/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/sharpen.jpg")
    # upscale("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/sharpen.jpg", "/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/upscale.jpg")
    try:
        barcode_data_arr=detect_barcode("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/")
        barcode_data=barcode_data_arr[0]
    except:
        barcode_data="Segment not found!"
    # barcode_data="zaglushka"
    # try:
    #     scanner = zbar.Scanner()
    #     sharpen("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/"+photoName, "/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/sharpened.jpg")
    #     upscale("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/sharpened.jpg", "/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/upscaled.jpg")
    #     barcodes=[]
    #     for filename in os.listdir("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/"):
    #         image_as_numpy_array = imread("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/upscaled.jpg")
    #         results = scanner.scan(image_as_numpy_array)
    #         try:
    #             barcodes.append(results[0].data.decode('ascii'))
    #         except:
    #             (print("detection failed"))
    #     print(barcodes)
    #     # for result in results:
    #     #     print(result.type, result.data.decode('ascii'))
    #     #     barcode1=result.data.decode('ascii')
        
    #     # #for i in range(5):
    #     # img=cv2.imread("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/"+photoName)
    #     # #results = zxingcpp.read_barcodes(img)
    #     # print("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/Barcode/"+photoName)
    #     # img = imutils.resize(img, width=512)
    #     # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #     # ret, img = cv2.threshold(img, 162, 255, cv2.THRESH_BINARY)
    #     # barcode=decode(img)
    #     # print(barcode)
    #     # #print(results[0].text)
    #     try:
    #         print("results of barcode", results[0].data.decode('ascii'))
    #         barcode_data=results[0].data.decode('ascii')
    #         #barcode_data=results[0].text
    #         # barcode_data=str(barcode[0].data)
    #         # barcode_data=barcode_data[2:-1]
    #     except:
    #         barcode_data="Bad image"
    # except:
    #     barcode_data="Segment not found!"

    # old qr
    try:
        qr_data_arr=detect_barcode("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/QR/")
        QR_data=qr_data_arr[0]
    except:
        QR_data="Segment not found!"
    # try:
    #     QR_img=cv2.imread("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/QR/"+photoName)
    #     QR_img = imutils.resize(QR_img, width=512)
    #     QR_img = cv2.cvtColor(QR_img, cv2.COLOR_BGR2GRAY)
    #     ret, img = cv2.threshold(QR_img, 162, 255, cv2.THRESH_BINARY)
    #     QRimg=decode(QR_img)
    #     try:
    #         QR_data=str(QRimg[0].data)
    #     except:
    #         QR_data="Bad image"
    # except:
    #     QR_data="Segment not found!"

    #END OF OLD QR
        # try:
    #    QR_img=cv2.imread("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/crops/QR/"+photoName)
    #    QR_img = imutils.resize(QR_img, width=600)
    #    QRimg=decode(QR_img)
    #    QR_data=str(QRimg[0].data)
    #    print(QR_data)
    #except:
    #    print("NO QR")

    #result = {'success': True, 'description': encrypt(descriptionAnswer), 'price11': encrypt(priceRubNoCardAnswer), 'price12': encrypt(priceKopAnswer), 'price21': encrypt(priceKopNoCardAnswer), 'price22': encrypt(priceRubAnswer), 'barcode_data': encrypt(data) }
    result = {'success': 'False', 'description': descriptionAnswer, 'price11': price11, 
    'price12': price12, 'price21': price21, 'price22': price22, 'barcode_data': barcode_data + " QR: " + QR_data + "\n",
    #'price_num_card': 'price_num_card', 'price_num_nocard': 'price_num_nocard', 'type': 'price_Type', 'numType': 'numType',
    #'price1c': 'None', 'description1c': 'None', 'price1cDiscount': 'None', 'Levi': 'None' }
    }
    # result = take_barcodes(result) #вызов метода, для связи с базой данных 1С
    # result['Levi']=fuzz.WRatio(result['description'],result['description1c']) #вычисление расстояния Левенштейна
    try:
        shutil.copy(pathToPhoto, "/home/sergei/TagAnalyzer/Django_server/content/dataset_expansion/images/")
        shutil.copy("/home/sergei/TagAnalyzer/Django_server/runs/detect/predict/labels/" + photoName[:-4] + ".txt", "/home/sergei/TagAnalyzer/Django_server/content/dataset_expansion/labels/")
    except:
        print("nothing found")
    print(result)
    shutil.rmtree("/home/sergei/TagAnalyzer/Django_server/runs/")
    os.remove(pathToPhoto) #удаляем полученное фото, можно оставить для расширения датасета
    return result

def detect_barcode(path_to_barcodes_folder):
    #pyzbar		
    detected_barcode=[]
    from pyzbar.pyzbar import decode
    barcode_fail=0
    barcode_succ=0

    for filename in os.listdir(path_to_barcodes_folder):
        img = cv2.imread(path_to_barcodes_folder+filename)
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        #pyzbar
        barcode=decode(img)
        if barcode != []:
            barcode_succ+=1
            detected_barcode.append(str(barcode[0].data)[2:-1])
        else:
            barcode_fail+=1
    print("pyzbar", "Detected:", barcode_succ, " Failed to detected:", barcode_fail)

    #zbarlight
    import zbarlight
    barcode_fail=0
    barcode_succ=0
    for filename in os.listdir(path_to_barcodes_folder):
        image=Image.open(path_to_barcodes_folder+filename)
        # image=ImageOps.grayscale(image)
        image.load()
        codes=zbarlight.scan_codes(['ean13'], image)
        if codes != None:
            barcode_succ+=1
            detected_barcode.append(str(codes[0])[2:-1])
            # print(filename)
        else:
            codes=zbarlight.scan_codes(['qrcode'], image)
            if codes != None:
                barcode_succ+=1
                detected_barcode.append(str(codes[0])[2:-1])

                print(filename)
            else:
                barcode_fail+=1
    print("zbarlight", "Detected:", barcode_succ, " Failed to detected:", barcode_fail)

    #zbar
    import zbar
    import zbar.misc
    barcode_fail=0
    barcode_succ=0
    scanner = zbar.Scanner()
    for filename in os.listdir(path_to_barcodes_folder):
        image_as_numpy_array = imread(path_to_barcodes_folder+filename)
        results = scanner.scan(image_as_numpy_array)
        if not results:
            barcode_fail+=1
            #print('No barcode found.')
        for result in results:
            if result.type == "EAN-13":
                barcode_succ+=1
                detected_barcode.append(result.data.decode('ascii'))
                # print(filename)
                # print(result.type, result.data.decode('ascii'))
            else:
                detected_barcode.append(result.data.decode('ascii'))
                #print("not ean")
    print("zbar", "Detected:", barcode_succ, " Failed to detected:", barcode_fail)

    #zxing-cpp
    import zxingcpp
    barcode_fail=0
    barcode_succ=0
    for filename in os.listdir(path_to_barcodes_folder):
        img = cv2.imread(path_to_barcodes_folder+filename)
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        results = zxingcpp.read_barcodes(img)
        if len(results)==0:
            barcode_fail+=1
        else:
            for result in results:
                detected_barcode.append(result.text)
            # detected_barcode.append(results)
            barcode_succ+=1
        #for result in results:
        #	print(result.text)
            #if barcode != []:
            #	barcode_succ+=1
            #else:
            #	barcode_fail+=1
    print("zxingcpp", "Detected:", barcode_succ, " Failed to detected:", barcode_fail)
    print("detected barcodes", detected_barcode)
    return detected_barcode

# Create an SR object
sr = dnn_superres.DnnSuperResImpl_create()
path = "/home/sergei/Документы/EDSR_x4.pb"
# Read the desired model
sr.readModel(path)
sr.setModel("edsr", 4)

#upscale
def upscale(path, pathtosave):
    
    image = cv2.imread(path)
    result = sr.upsample(image)
    cv2.imwrite(pathtosave, result)

#sharpening
def sharpen(path, pathtosave):
    imageObject = Image.open(path)
    sharpened1 = imageObject.filter(ImageFilter.SHARPEN)
    sharpened1.save(pathtosave)

def imread(image_filename):
        '''Example image-reading function that tries to use freeimage, skimage, scipy or pygame to read in an image'''

        try:
            from freeimage import read as read_image
        except ImportError:
            read_image = None

        if read_image is None:
            try:
                from skimage.io import imread as read_image
            except ImportError:
                pass

        if read_image is None:
            try:
                from scipy.misc import imread as read_image
            except ImportError:
                pass

        if read_image is None:
            try:
                import pygame.image
                import pygame.surfarray
                def read_image(image_filename):
                    image_pygame_surface = pygame.image.load(image_filename)
                    return pygame.surfarray.array3d(image_pygame_surface)
            except ImportError:
                raise ImportError('for this example freeimage, skimage, scipy, or pygame are required for image reading')

        image = read_image(image_filename)
        if len(image.shape) == 3:
            image = zbar.misc.rgb2gray(image)
        return image

#шифрование
def pad(byte_array):
    BLOCK_SIZE = 16
    pad_len = BLOCK_SIZE - len(byte_array) % BLOCK_SIZE
    return byte_array + (bytes([pad_len]) * pad_len)
    
def unpad(byte_array):
    last_byte = byte_array[-1]
    return byte_array[0:-last_byte]
    
def encrypt(message1):
    iv = b'This is a key123'
    key = 'This is a key123'
    byte_array = message1.encode("UTF-8")
    padded = pad(byte_array)
    obj = AES.new(key.encode("UTF-8"), AES.MODE_CBC,iv)
    ciphertext1 = obj.encrypt(padded)
    ct1 = base64.b64encode(ciphertext1).decode("UTF-8")
    return ct1

#вычисление цены за единицу
def PricePerNum(descryption, price11, price12, price21, price22):
    #словарь для замены букв в предложении на похожие цифры
    dictLetterToNum = {'O':0, 'S':5, 'Б':5, 'Z':2,'О':0,'б':6,'З':3, ' ': '', 'А':4, 'A':4 }
    prices_per_num = []
    #регулярка, которая ищет цену в названии товара, предварительно буквы заменяются на цифры в соответствии со словарём,
    #из названиря убираются все пробелы
    priceValues=re.findall(
        r'(?:(?:\d*\.{1})|(?:\d+\+)+)?\d+\s*(?:мл|шт|гр|L|литр(?:а|ов)?|кг|мг|г|пар(?:а|ы)?|пак|ш|Л|л)+', 
        replaceLetterToNum(descryption,dictLetterToNum)
    )
    if len(priceValues)!=0: #определил хотя бы одну пару число/единица измерения
        num=getNumOfType(priceValues[-1]) #берём последнее значение из списка для цены
        type = getType(priceValues[-1]) #берём последнее значение для единицы
        #type = ''.join(getType(priceValues[-1])[len(getType(priceValues[-1]))-1]) #wtf?
        #обработка маркетологов 2+1
        try: #если нашлась строка с плюсом, то делим и получаем значения из него, формируем новый список с ними
            num=str(num[-1]).split('+')
        except (Exception): #может исключения и не возникает но на всякий случай я сделал отлов
            print("Faker")
        if len(num)!=1: #если нашлось несколько значений (2+1 etc), т.е. результат разделения выше
            finalNum=0 #начальная сумма 0
            for i in num:
                finalNum=finalNum+int(i) #суммируем
        else: #значение всего одно, его и берём (если это десятичное число, то переводим в него)
            try: 
                finalNum=int(num[0])
            except Exception:
                finalNum=float(num[0])
        #вычисление значений цен
        try:
            priceNoCard = float(price11 + '.' + price21)
            priceCard = float(price22 + '.' + price12)
            prices_per_num.append(str(round(priceCard/finalNum, 4))) #цена по карте/единица
            prices_per_num.append(str(round(priceNoCard/finalNum, 4))) #цена без карты/единица
            prices_per_num.append(str(type)) #единицы измерения
            prices_per_num.append(str(priceValues[-1])) #цена вместе с единицей
        except (Exception): #при делении на ноль осознаём, что нейронка ошиблась и пишем, что метрики не найдены
            for i in range(1,4):
                prices_per_num.append("no metric")
        return (prices_per_num)
                 
    else: #нейронка не нашла читаемых единиц, пишем, что метрики не найдены
        for i in range(1,4):
                prices_per_num.append("no metric")
        return (prices_per_num)

def getNumOfType(targetString):
    return(re.findall(r'(?:(?:\d*\.{1})|(?:\d+\+)+)?\d+', targetString))

def getType(targetString):
    return(re.findall(r'(?:мл|шт|гр|L|литр(?:а|ов)?|кг|мг|г|пар(?:а|ы)?|пак|ш|Л|л)+', targetString))
    
def replaceLetterToNum(targetString, replace_values):
    for key in replace_values.keys():
      targetString = targetString.replace(key, str(replace_values[key]))
    return targetString
